import Kredit from '../domain/Kredit';
import Iznos from '../domain/Iznos';
import PeriodOtplate from '../domain/PeriodOtplate';
import KamatnaStopa from '../domain/KamatnaStopa';
import Ucesce from '../domain/Ucesce';

export const sviKrediti = [
    new Kredit(
        new Iznos(15000, 1200000, 5000),
        new PeriodOtplate(6, 72, 1),
        null,
        new KamatnaStopa(
            [4.99, 9.86],
            [
                ['6 - 12', [4.99, 9.86]],
                ['13 - 36', [8.48, 9.63]],
                ['37 - 72', [10.99, 12.01]],
            ]
        ),
        'Dinarski kredit sa fiksnom kamatnom stopom',
        'dinarski-kredit-sa-fiksnom-kamatnom-stopom'
    ),
    // Ovde treba da ide onaj koji fali
    new Kredit(
        new Iznos(400, 50000, 50, 'EUR'),
        new PeriodOtplate(6, 84, 1),
        null,
        new KamatnaStopa([3.49, 4.93], [['6 - 84', [3.49, 4.93]]]),
        'Dinarski kredit sa valutnom klauzulom i 100% depozitom',
        'dinarski-kredit-sa-valutnom-klauzulom-i-100-depozitom'
    ),
    new Kredit(
        new Iznos(10000, 1200000, 5000),
        new PeriodOtplate(6, 72, 1),
        null,
        new KamatnaStopa(
            [4.84, 7.63],
            [
                ['6 - 12', [4.84, 7.63]],
                ['13 - 36', [7.48, 8.54]],
                ['37 - 72', [10.99, 12.01]],
            ]
        ),
        'Dinarski potrošački krediti sa fiksnom kamatnom stopom',
        'dinarski-potrosacki-krediti-sa-fiksnom-kamatnom-stopom'
    ),
    new Kredit(
        new Iznos(10000, 1200000, 5000),
        new PeriodOtplate(6, 72, 1),
        null,
        new KamatnaStopa(
            [5.05, 9.56],
            [
                ['6 - 12', [5.05, 9.56]],
                ['13 - 36', [8.95, 10.21]],
                ['37 - 72', [10.9, 11.95]],
            ]
        ),
        'Start-up family krediti za vantelesnu oplodnju',
        'start-up-family-krediti-za-vantelesnu-oplodnju'
    ),
    new Kredit(
        new Iznos(15000, 1200000, 5000),
        new PeriodOtplate(6, 72, 1),
        null,
        new KamatnaStopa(
            [4.99, 9.86],
            [
                ['6 - 12', [4.99, 9.86]],
                ['13 - 36', [8.48, 9.63]],
                ['37 - 72', [10.99, 12.01]],
            ]
        ),
        'Dinarski krediti za refinansiranje sa fiksnom kamatnom stopom',
        'dinarski-krediti-za-refinansiranje-sa-fiksnom-kamatnom-stopom'
    ),
    new Kredit(
        new Iznos(150, 12000, 50, 'EUR'),
        new PeriodOtplate(6, 72, 1),
        null,
        new KamatnaStopa([7.0, 8.15], [['6 - 72', [7.0, 8.15]]]),
        'Dinarski kredit za refinansiranje sa valutnom klauzulom',
        'dinarski-kredit-za-refinansiranje-sa-valutnom-klauzulom'
    ),
    new Kredit(
        new Iznos(50000, 10000000, 5000),
        new PeriodOtplate(6, 84, 1),
        null,
        new KamatnaStopa([10.09, 10.94], [['6 - 84', [10.09, 10.94]]]),
        'Dinarski auto kredit sa fiksnom kamatnom stopom',
        'dinarski-auto-kredit-sa-fiksnom-kamatnom-stopom'
    ),
    new Kredit(
        new Iznos(500, 20000, 100, 'EUR'),
        new PeriodOtplate(6, 84, 1),
        new Ucesce(30, 30, 1, false),
        new KamatnaStopa([6.5, 6.84], [['6 - 84', [6.5, 6.84]]]),
        'Dinarski auto kredit sa valutnom klauzulom',
        'dinarski-auto-krediti-sa-valutnom-klauzulom'
    ),
    new Kredit(
        new Iznos(1000000, 10000000, 10000),
        new PeriodOtplate(6, 84, 1),
        new Ucesce(25, 80, 1),
        new KamatnaStopa([10.99, 13.29], [['6 - 84', [10.99, 13.29]]]),
        'Dinarski stambeni krediti sa fiksnom kamatnom stopom',
        'dinarski-stambeni-krediti-sa-fiksnom-kamatnom-stopom'
    ),
    new Kredit(
        new Iznos(10000, 500000, 1000, 'EUR'),
        new PeriodOtplate(6, 240, 1),
        new Ucesce(25, 80, 1),
        new KamatnaStopa([6.49, 7.35], [['6 - 240', [6.49, 7.35]]]),
        'Dinarski stambeni krediti sa valutnom klauzulom',
        'dinarski-stambeni-krediti-sa-valutnom-klauzulom'
    ),
    new Kredit(
        new Iznos(1000000, 10000000, 1000),
        new PeriodOtplate(6, 84, 1),
        null,
        new KamatnaStopa([11.99, 12.87], [['6 - 240', [11.99, 12.87]]]),
        'Dinarski kredit za adaptaciju, rekonstrukciju i proširenje stambenog prostora sa fiksnom kamatnom stopom',
        'dinarski-kredit-za-adaptaciju-rekonstrukciju-i-prosirenje-stambenog-prostora-sa-fiksnom-kamatnom-stopom'
    ),
    new Kredit(
        new Iznos(10000, 100000, 1000, 'EUR'),
        new PeriodOtplate(6, 240, 1),
        null,
        new KamatnaStopa([6.99, 7.12], [['6 - 240', [6.99, 7.12]]]),
        'Dinarski kredit za adaptaciju, rekonstrukciju i proširenje stambenog prostora sa valutnom klauzulom',
        'dinarski-kredit-za-adaptaciju-rekonstrukciju-i-prosirenje-stambenog-prostora-sa-valutnom-klauzulom'
    ),
];
