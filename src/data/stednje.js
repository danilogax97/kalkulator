import { Stednja } from '../domain/Stednja';
import IznosStednja from '../domain/IznosStednja';
import KamatnaStopaStednja from '../domain/KamatnaStopaStednja';

export const sveStednje = [
    new Stednja(
        new IznosStednja(1000, 1000000, 100, ['EUR']),
        [12],
        'Rentna štednja',
        new KamatnaStopaStednja({
            12: { EUR: { stopa: 1.2, efektivna: 1.02, stopaPoreza: 15 } },
        }),
        'rentna-stednja'
    ),
    new Stednja(
        new IznosStednja(100, 1000000, 100, ['EUR', 'USD', 'CHF']),
        [1, 3, 6, 9, 12, 15, 18, 24, 25, 36],
        'Oročeni devizni depoziti',
        new KamatnaStopaStednja({
            1: {
                EUR: {
                    stopa: 0.1,
                    efektivna: 0.08,
                    stopaPoreza: 15,
                },
            },
            3: {
                EUR: {
                    stopa: 0.5,
                    efektivna: 0.42,
                    stopaPoreza: 15,
                },
                USD: {
                    stopa: 0.4,
                    efektivna: 0.34,
                    stopaPoreza: 15,
                },
                CHF: {
                    stopa: 0.01,
                    efektivna: 0.01,
                    stopaPoreza: 15,
                },
            },
            6: {
                EUR: {
                    stopa: 0.7,
                    efektivna: 0.59,
                    stopaPoreza: 15,
                },
                USD: {
                    stopa: 0.8,
                    efektivna: 0.68,
                    stopaPoreza: 15,
                },
                CHF: {
                    stopa: 0.01,
                    efektivna: 0.01,
                    stopaPoreza: 15,
                },
            },
            9: {
                EUR: {
                    stopa: 0.9,
                    efektivna: 0.76,
                    stopaPoreza: 15,
                },
            },
            12: {
                EUR: {
                    stopa: 1.2,
                    efektivna: 1.02,
                    stopaPoreza: 15,
                },
                USD: {
                    stopa: 1.4,
                    efektivna: 1.19,
                    stopaPoreza: 15,
                },
                CHF: {
                    stopa: 0.01,
                    efektivna: 0.01,
                    stopaPoreza: 15,
                },
            },
            15: {
                EUR: {
                    stopa: 1.4,
                    efektivna: 1.19,
                    stopaPoreza: 15,
                },
            },
            18: {
                EUR: {
                    stopa: 1.4,
                    efektivna: 1.19,
                    stopaPoreza: 15,
                },
            },
            24: {
                EUR: {
                    stopa: 1.5,
                    efektivna: 1.28,
                    stopaPoreza: 15,
                },
                USD: {
                    stopa: 1.7,
                    efektivna: 1.45,
                    stopaPoreza: 15,
                },
            },
            25: {
                EUR: {
                    stopa: 1.6,
                    efektivna: 1.36,
                    stopaPoreza: 15,
                },
            },
            36: {
                EUR: {
                    stopa: 1.8,
                    efektivna: 1.51,
                    stopaPoreza: 15,
                },
                USD: {
                    stopa: 1.7,
                    efektivna: 1.45,
                    stopaPoreza: 15,
                },
            },
        }),
        'oroceni-devizni-depoziti'
    ),
    new Stednja(
        new IznosStednja(5000, 1000000, 100, ['RSD']),
        [2, 3, 6, 12, 24, 36],
        'Oročena dinarska štednja',
        new KamatnaStopaStednja({
            2: {
                RSD: {
                    stopa: 1.75,
                    efektivna: 1.75,
                    stopaPoreza: 0,
                },
            },
            3: {
                RSD: {
                    stopa: 2,
                    efektivna: 2,
                    stopaPoreza: 0,
                },
            },
            6: {
                RSD: {
                    stopa: 3,
                    efektivna: 3,
                    stopaPoreza: 0,
                },
            },
            12: {
                RSD: {
                    stopa: 3.5,
                    efektivna: 3.5,
                    stopaPoreza: 0,
                },
            },
            24: {
                RSD: {
                    stopa: 3.75,
                    efektivna: 3.75,
                    stopaPoreza: 0,
                },
            },
            36: {
                RSD: {
                    stopa: 4,
                    efektivna: 4,
                    stopaPoreza: 0,
                },
            },
        }),
        'orocena-dinarska-stednja'
    ),
    new Stednja(
        new IznosStednja(5000, 1000000, 100, ['RSD', 'EUR']),
        [6, 12],
        'Dečja štednja',
        new KamatnaStopaStednja({
            6: {
                RSD: {
                    stopa: 3.25,
                    efektivna: 3.25,
                    stopaPoreza: 0,
                },
                EUR: {
                    stopa: 0.8,
                    efektivna: 0.8,
                    stopaPoreza: 15,
                },
            },
            12: {
                RSD: {
                    stopa: 3.75,
                    efektivna: 3.75,
                    stopaPoreza: 0,
                },
                EUR: {
                    stopa: 1.3,
                    efektivna: 1.3,
                    stopaPoreza: 15,
                },
            },
        }),
        'decja-stednja'
    ),
];
