/**
 *
 * @param {number} value Predstavlja broj elemenata
 * @param {string} singular
 * @param {string} plural1
 * @param {string} plural2
 *
 * @returns {string}
 */
export const pluralSingular = (value, singular, plural1, plural2 = plural1) => {
    switch (value) {
        case 1:
            return singular;
        case 2:
        case 3:
        case 4:
            return plural1;
        default:
            return plural2;
    }
};

/**
 *
 * @param {number} value
 * @param {string} singular
 * @param {string} plural1
 * @param {string} plural2
 *
 * @returns {string}
 */
export const formatNumberWithUnit = (
    value,
    singular,
    plural1,
    plural2 = plural1
) => {
    const unit = pluralSingular(value, singular, plural1, plural2);

    return `${value} ${unit}`;
};

/**
 *
 * @param {number} value
 * @param {number} numDecimals
 * @param {string} prefix
 *
 * @returns {string}
 */
export const formatPercent = (value, numDecimals = 2, prefix = '*') => {
    return `${prefix}${value.toFixed(numDecimals)}%`;
};

/**
 *
 * @param {number} value
 * @param {string} currency
 */
export const formatCurrency = (value, currency) => {
    const format = new Intl.NumberFormat('en-DE', {maximumFractionDigits: 2});

    return `${format.format(value)} ${currency}`;
};
