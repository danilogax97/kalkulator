import { sveStednje } from '../data/stednje';
import Kalkulator from './Kalkulator';
import {
    getEditableRow,
    getNotEditableRow,
    getSelectRow,
    getSmallerRow,
} from '../generators/htmlPartials';
import {
    updateLabels,
    updateProgress,
    updateValue,
    updateValuePosition,
} from '../sliderHelpers';
import {
    formatCurrency,
    formatNumberWithUnit,
    formatPercent,
    pluralSingular,
} from '../textHelpers';

export class KalkulatorStednje extends Kalkulator {
    /**
     * @param {HTMLDivElement} mainContainer
     * @param {string} nazivIzabraneStavke
     */
    constructor(mainContainer, nazivIzabraneStavke) {
        super(mainContainer, '-stednja', sveStednje, nazivIzabraneStavke);
    }

    calculate() {
        const periodOrocenja = parseInt(this.selectedPeriodOrocenja); // u mesecima
        const nks = this.selectedKamatnaStopa.stopa; // procenti
        const stopaPoreza = this.selectedKamatnaStopa.stopaPoreza; // procenti
        const depozit = parseInt(this.selectedIznos);

        const obracunKamate = (depozit * nks * periodOrocenja) / 1200; // 12 * 100 (jer je nks u procentima)
        const obracunPoreza = (obracunKamate * stopaPoreza) / 100; // jer je stopaPoreza u procentima

        return {
            kamata: obracunKamate - obracunPoreza,
            iznosZaIsplatu: depozit + obracunKamate - obracunPoreza,
        };
    }

    populateScreen1() {
        const izabranaStednja = this.izabraniPodatak();

        if (!izabranaStednja.imaViseValuta()) {
            const currencyRowContainer = getNotEditableRow(
                'Valuta',
                'valuta',
                izabranaStednja.iznos.valuta
            );
            this.rowsContainer.appendChild(currencyRowContainer);
            this.selectedValuta = izabranaStednja.iznos.valuta;
        } else {
            const { rowContainer: currencyRowContainer, select } = getSelectRow(
                {
                    podaci: izabranaStednja.iznos.valute,
                    labelText: 'Valuta',
                    onValueChanged: this.onIzabranaValuta.bind(this),
                    mapping: (podatak) => ({ text: podatak, value: podatak }),
                    id: `${this.id}-suffix-valuta`,
                    selectedValueId: '-valuta-stednje',
                }
            );
            this.currencySelect = select;
            this.selectedValuta = this.currencySelect.getSelected();
            this.rowsContainer.appendChild(currencyRowContainer);
        }

        const iznosDepozitaRowContainer = getEditableRow({
            field: izabranaStednja.iznos,
            labelText: 'Iznos depozita',
            inputLabelText: 'Unesite iznos depozita',
            id: '-iznos-depozita',
            minValueSuffix: `${this.selectedValuta}`,
            maxValueSuffix: ` ${this.selectedValuta}`,
        });

        this.rowsContainer.appendChild(iznosDepozitaRowContainer);

        if (!izabranaStednja.imaVisePeriodaOrocenja()) {
            const periodOrocenjaRowContainer = getNotEditableRow(
                'Period oročenja',
                'period-orocenja',
                izabranaStednja.periodOrocenja
            );
            this.selectedPeriodOrocenja = izabranaStednja.periodOrocenja;
            this.rowsContainer.appendChild(periodOrocenjaRowContainer);
        } else {
            const {
                rowContainer: periodiOrocenjaRowContainer,
                select,
            } = getSelectRow({
                podaci: izabranaStednja.dostupniPeriodiOrocenja(
                    this.selectedValuta
                ),
                labelText: 'Period oročenja',
                onValueChanged: this.onIzabranPeriodOrocenja.bind(this),
                mapping: (podatak) => ({ text: podatak, value: podatak }),
                valueSuffix: pluralSingular(
                    izabranaStednja.periodOrocenja,
                    'mesec',
                    'meseca',
                    'meseci'
                ),
                id: `${this.id}-suffix-period-orocenja`,
                selectedValueId: '-period-orocenja',
            });
            this.periodOrocenjaSelect = select;
            this.selectedPeriodOrocenja = this.periodOrocenjaSelect.getSelected();
            this.rowsContainer.appendChild(periodiOrocenjaRowContainer);
        }

        const { stopa, efektivna, stopaPoreza } = this.selectedKamatnaStopa;

        const nksRowContainer = getNotEditableRow(
            'NKS',
            'nks-stednja',
            formatPercent(stopa)
        );

        const eksRowContainer = getNotEditableRow(
            'EKS',
            'eks-stednja',
            formatPercent(efektivna)
        );

        const stopaPorezaRowContainer = getNotEditableRow(
            'Stopa poreza',
            'stopa-poreza-stednja',
            formatPercent(stopaPoreza)
        );

        this.rowsContainer.append(
            nksRowContainer,
            eksRowContainer,
            stopaPorezaRowContainer
        );
        this.nksStednja = document.getElementById('nks-stednja');
        this.eksStednja = document.getElementById('eks-stednja');
        this.stopaPoreza = document.getElementById('stopa-poreza-stednja');

        //this.enableButton();
    }

    /**
     *
     * @returns {{stopa: number, efektivna: number, stopaPoreza: number}}
     */
    get selectedKamatnaStopa() {
        const izabranaStednja = this.izabraniPodatak();

        return (
            izabranaStednja.kamatnaStopa.config[this.selectedPeriodOrocenja][
                this.selectedValuta
            ] || null
        );
    }

    get selectedIznos() {
        const slider = document.getElementById(
            'tick-slider-input-iznos-depozita'
        );

        return slider.value;
    }

    updateKamatnaStopa() {
        if (this.selectedKamatnaStopa) {
            const { stopa, efektivna, stopaPoreza } = this.selectedKamatnaStopa;
            this.nksStednja.textContent = formatPercent(stopa);
            this.eksStednja.textContent = formatPercent(efektivna);
            this.stopaPoreza.textContent = formatPercent(stopaPoreza);
            //this.enableButton();
        } else {
            this.nksStednja.textContent = '/';
            this.eksStednja.textContent = '/';
            //this.disableButton();
        }
    }

    // disableButton() {
    //     this.button.disabled = true;
    // }
    //
    // enableButton() {
    //     this.button.disabled = false;
    // }

    /**
     * @param {object} calculationResult
     * @param {number} calculationResult.kamata
     * @param {number} calculationResult.iznosZaIsplatu
     */
    populateScreen2({ kamata, iznosZaIsplatu }) {
        const izabranaStednja = this.izabraniPodatak();

        const {
            stopa: nks,
            efektivna: eks,
            stopaPoreza,
        } = this.selectedKamatnaStopa;

        const row1 = getSmallerRow('Vrsta štednje', izabranaStednja.naziv);
        const row2 = getSmallerRow('Valuta štednje', this.selectedValuta);
        const row3 = getSmallerRow('Stopa poreza', formatPercent(stopaPoreza));
        const row4 = getSmallerRow(
            'Iznos depozita',
            formatCurrency(parseInt(this.selectedIznos), this.selectedValuta)
        );
        const row5 = getSmallerRow(
            'Period oročenja',
            formatNumberWithUnit(
                parseInt(this.selectedPeriodOrocenja),
                'mesec',
                'meseca',
                'meseci'
            )
        );
        const row6 = getSmallerRow(
            'Iznos za isplatu',
            formatCurrency(iznosZaIsplatu, this.selectedValuta)
        );
        const row7 = getSmallerRow(
            'Kamata za isplatu',
            formatCurrency(kamata, this.selectedValuta)
        );
        const row8 = getSmallerRow('NKS', formatPercent(nks));
        const row9 = getSmallerRow('EKS', formatPercent(eks));

        this.screen2Rows.append(
            row1,
            row2,
            row3,
            row4,
            row5,
            row6,
            row7,
            row8,
            row9
        );
    }

    /**
     * @param {string} valuta
     */
    onIzabranaValuta(valuta) {
        this.selectedValuta = valuta;
        this.updatePeriodOrocenjaSelectOptions();
        this.updateKamatnaStopa();
        this.updateIznosSliderSuffix();
    }

    updatePeriodOrocenjaSelectOptions() {
        if (this.periodOrocenjaSelect) {
            const izabranaStednja = this.izabraniPodatak();

            this.periodOrocenjaSelect.podaci = izabranaStednja.dostupniPeriodiOrocenja(
                this.selectedValuta
            );
            this.periodOrocenjaSelect.populate((podatak) => ({
                text: podatak,
                value: podatak,
            }));
            this.periodOrocenjaSelect.bindOptionsEvents();
            this.selectedPeriodOrocenja = this.periodOrocenjaSelect.getSelected();
        }
    }

    /**
     *
     * @param {string} periodOrocenja
     */
    onIzabranPeriodOrocenja(periodOrocenja) {
        this.selectedPeriodOrocenja = periodOrocenja;
        this.updateKamatnaStopa();
        this.updatePeriodOrocenjaSelectValueSuffix();
    }

    updatePeriodOrocenjaSelectValueSuffix() {
        const periodOrocenjaSuffix = document.getElementById(
            `${this.id}-suffix-period-orocenja`
        );

        periodOrocenjaSuffix.textContent = pluralSingular(
            parseInt(this.selectedPeriodOrocenja),
            'mesec',
            'meseca',
            'meseci'
        );
    }

    updateIznosSliderSuffix() {
        /**
         * TODO:    Takodje treba da se updejtuje i minimalan iznos
         *          koji se obracunava kao iznos
         *          100EUR u protivvrednosti
         *          valute koja je izabrana
         */

        const izabranaStednja = this.izabraniPodatak();

        const slider = document.getElementById(
            'tick-slider-input-iznos-depozita'
        );

        const weightValueMin = document.getElementById(
            'weightLabelMin-iznos-depozita'
        );
        const weightValueMax = document.getElementById(
            'weightLabelMax-iznos-depozita'
        );

        slider.setAttribute('data-suffix', ` ${this.selectedValuta}`);
        weightValueMin.textContent = formatCurrency(
            izabranaStednja.iznos.min,
            this.selectedValuta
        );

        weightValueMax.textContent = formatCurrency(
            izabranaStednja.iznos.max,
            this.selectedValuta
        );

        updateValue(slider);
        updateValuePosition(slider);
        updateLabels(slider);
        updateProgress(slider);
    }
}
