import SelectField from '../generators/SelectField';
import backArrowIcon from '../generators/backArrowIcon';
import { init } from '../sliderHelpers';

export default class Kalkulator {
    /**
     * @param {HTMLDivElement} mainContainer
     * @param {string} id
     * @param {Stednja[] | Kredit[]} podaci
     * @param {string} idIzabraneStavke
     */
    constructor(mainContainer, id, podaci, idIzabraneStavke) {
        this.mainContainer = mainContainer;
        this.id = id;
        this.podaci = podaci;

        this.select = new SelectField(
            this.podaci,
            this.id,
            this.onPodatakIzabran.bind(this),
            this.chooseStavka(idIzabraneStavke)
        );

        this.createScreen1();
        this.createScreen2();

        this.mainContainer.appendChild(this.screen1);
        this.mainContainer.appendChild(this.screen2);
    }

    /**
     *
     * @param {string} idStavke
     */
    chooseStavka(idStavke) {
        const index = this.podaci.findIndex(
            (podatak) => podatak.id === idStavke
        );

        return index !== -1 ? index : 0;
    }

    /**
     * Treba da se implementira u subklasama.
     *
     * @returns {object} Objekat koji je rezultat kalkulacije i koji je proslednjen metodi populateScreen2()
     * @see populateScreen2
     * @see switchScreen
     */
    calculate() {
        console.log('calculating');

        return {};
    }

    /**
     * Treba da se implementira u subklasama.
     *
     * @param {object} calculationResult Resultat izracunavanja koji vraca calculate()
     * @see calculate
     * @see switchScreen
     */
    populateScreen2(calculationResult) {
        console.log('populating screen2');
    }

    /**
     * Treba da se implementira u sub klasama.
     */
    populateScreen1() {
        console.log('handling field generation');
    }

    onPodatakIzabran() {
        // generisanje polja za izabrani kredit
        const izabraniPodatak = this.izabraniPodatak();
        if (izabraniPodatak) {
            this.rowsContainer.innerHTML = '';
            // kada izaberemo novi kredit opet generisi redove
            this.populateScreen1();
            init(this.screen1);
        }
    }

    createScreen1() {
        this.screen1 = document.createElement('div');

        this.screen1.setAttribute('id', `screen1${this.id}`);
        this.screen1.classList.add('show');

        this.rowsContainer = document.createElement('div');
        this.rowsContainer.setAttribute('id', `rows-container${this.id}`);

        this.button = document.createElement('button');
        this.button.setAttribute('id', 'mainButton');
        this.button.addEventListener('click', this.switchScreen.bind(this));
        this.button.innerText = 'IZRAČUNAJ';

        this.screen1.appendChild(this.select.html);
        this.screen1.appendChild(this.rowsContainer);
        this.screen1.appendChild(this.button);
    }

    switchScreen() {
        if (this.screen1.classList.contains('show')) {
            this.screen1.classList.replace('show', 'hide');
            this.screen2.classList.replace('hide', 'show');

            const calculationResultData = this.calculate();

            this.screen2Rows.innerHTML = '';
            this.populateScreen2(calculationResultData);
        } else {
            this.screen2.classList.replace('show', 'hide');
            this.screen1.classList.replace('hide', 'show');
        }
    }

    createScreen2(
        onRightClick = () => console.log('right click'),
        onLeftClick = () => console.log('left click')
    ) {
        this.screen2 = document.createElement('div');

        this.screen2.setAttribute('id', `screen2${this.id}`);
        this.screen2.classList.add('hide');

        const navigation = document.createElement('div');
        navigation.classList.add('navigation');
        navigation.addEventListener('click', this.switchScreen.bind(this));

        const navigationText = document.createElement('h2');
        navigationText.classList.add('nav-text');
        navigationText.textContent = 'IZRAČUNAJ PONOVO';

        navigation.innerHTML = backArrowIcon;
        navigation.appendChild(navigationText);

        this.screen2Rows = document.createElement('div');
        this.screen2Rows.setAttribute('id', `screen2-rows${this.id}`);

        const buttonsContainer = document.createElement('div');
        buttonsContainer.classList.add('button-wrapper');

        const secondaryButton = document.createElement('button');
        secondaryButton.classList.add('main-button', 'secondary');
        secondaryButton.innerHTML = 'APLICIRAJ ONLINE';
        secondaryButton.addEventListener('click', onLeftClick);

        const mainButton = document.createElement('button');
        mainButton.classList.add('main-button');
        mainButton.innerHTML = 'ZAKAŽI SASTANAK';
        mainButton.addEventListener('click', onRightClick);

        buttonsContainer.appendChild(secondaryButton);
        buttonsContainer.appendChild(mainButton);

        // add all elements
        this.screen2.appendChild(navigation);
        this.screen2.appendChild(this.screen2Rows);
        this.screen2.appendChild(buttonsContainer);
    }

    /**
     *
     * @returns {Stednja | Kredit}
     */
    izabraniPodatak() {
        const nazivIzabranogPodatka = this.select.getSelected();

        return this.podaci.find(
            (podatak) => podatak.naziv === nazivIzabranogPodatka
        );
    }

    init() {
        this.select.init();
        this.populateScreen1();
    }

    show() {
        if (this.screen1.classList.contains('hide'))
            this.screen1.classList.replace('hide', 'show');
        if (this.screen1.classList.contains('hide'))
            this.screen1.classList.replace('hide', 'show');
    }

    hide() {
        if (this.screen1.classList.contains('show'))
            this.screen1.classList.replace('show', 'hide');
        if (this.screen2.classList.contains('show'))
            this.screen2.classList.replace('show', 'hide');
    }
}
