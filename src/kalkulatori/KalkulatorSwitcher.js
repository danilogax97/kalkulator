import KalkulatorKredita from './KalkulatorKredita';
import { KalkulatorStednje } from './KalkulatorStednje';
import { init, onResize } from '../sliderHelpers';

export default class KalkulatorSwitcher {
    /**
     * @param {HTMLDivElement} mainContainer
     * @param {string} izabraniKalkulatorName
     * @param {string} idIzabraneStavke
     */
    constructor(mainContainer, izabraniKalkulatorName, idIzabraneStavke) {
        this.mainContainer = mainContainer;

        this.createTopBar();
        this.kalkulatorKredita = new KalkulatorKredita(
            mainContainer,
            idIzabraneStavke
        );
        this.kalkulatorStednje = new KalkulatorStednje(
            mainContainer,
            idIzabraneStavke
        );
        this.izabraniKalkulator = izabraniKalkulatorName;
    }

    createTopBar() {
        const kalkSelektorContainer = document.createElement('div');
        kalkSelektorContainer.setAttribute('id', 'kalk-selektor');
        kalkSelektorContainer.classList.add('row', 'transparent', 'no-p-left');

        this.kreditKalkSelectButton = document.createElement('button');
        this.kreditKalkSelectButton.textContent = 'Kalkulator kredita';
        this.kreditKalkSelectButton.classList.add('top-bar-button');
        this.kreditKalkSelectButton.setAttribute(
            'data-kalk',
            'kalkulator-kredita'
        );
        this.kreditKalkSelectButton.addEventListener(
            'click',
            this.switchKalk.bind(this)
        );

        const verticalBar1 = document.createElement('div');
        verticalBar1.classList.add('vertical-bar');

        this.stednjaKalkSelectButton = document.createElement('button');
        this.stednjaKalkSelectButton.textContent = 'Kalkulator štednje';
        this.stednjaKalkSelectButton.classList.add('top-bar-button');
        this.stednjaKalkSelectButton.setAttribute(
            'data-kalk',
            'kalkulator-stednje'
        );
        this.stednjaKalkSelectButton.addEventListener(
            'click',
            this.switchKalk.bind(this)
        );

        const verticalBar2 = document.createElement('div');
        verticalBar2.classList.add('vertical-bar');

        this.zaduzenjaKalkSelectButton = document.createElement('button');
        this.zaduzenjaKalkSelectButton.textContent =
            'Kalkulator maksimalnog zaduženja';
        this.zaduzenjaKalkSelectButton.classList.add('top-bar-button');
        // switchKalk za za treci kalkulator kad bude bilo

        this.setActiveButtonClass();

        kalkSelektorContainer.appendChild(this.kreditKalkSelectButton);
        kalkSelektorContainer.appendChild(verticalBar1);
        kalkSelektorContainer.appendChild(this.zaduzenjaKalkSelectButton);
        kalkSelektorContainer.appendChild(verticalBar2);
        kalkSelektorContainer.appendChild(this.stednjaKalkSelectButton);

        this.mainContainer.appendChild(kalkSelektorContainer);
    }

    setActiveButtonClass() {
        switch (this.izabraniKalkulator) {
            case 'kalkulator-kredita':
                this.kreditKalkSelectButton.classList.add('active');
                this.stednjaKalkSelectButton.classList.remove('active');
                this.zaduzenjaKalkSelectButton.classList.remove('active');
                break;
            case 'kalkulator-stednje':
                this.stednjaKalkSelectButton.classList.add('active');
                this.kreditKalkSelectButton.classList.remove('active');
                this.zaduzenjaKalkSelectButton.classList.remove('active');
                break;
            case 'kalkulator-zaduzenja':
                this.zaduzenjaKalkSelectButton.classList.add('active');
                this.kreditKalkSelectButton.classList.remove('active');
                this.stednjaKalkSelectButton.classList.remove('active');
                break;
        }
    }

    switchKalk({ target }) {
        this.izabraniKalkulator = target.getAttribute('data-kalk');

        this.chooseKalk();

        this.setActiveButtonClass();
    }

    chooseKalk() {
        switch (this.izabraniKalkulator) {
            case 'kalkulator-stednje':
                this.kalkulatorKredita.hide();
                this.kalkulatorStednje.show();
                break;
            case 'kalkulator-maksimalnog-zaduzenja':
                break;
            case 'kalkulator-kredita':
            default:
                this.kalkulatorStednje.hide();
                this.kalkulatorKredita.show();
                break;
        }

        this.setActiveButtonClass();
    }

    initKalks() {
        this.kalkulatorKredita.init();
        this.kalkulatorStednje.init();

        window.addEventListener('load', this.onLoad.bind(this));
        window.addEventListener('resize', onResize);
    }

    onLoad() {
        /**
         * HACK: Moraju prvo oba da se pokazu da bi se slajderi lepo inicializovali na oba kalkulatora.
         * U suprotnom ce slajderi da se inicijalizuju lepo samo na onom kalkulatoru koji je prikazan na pocetku.
         * Razlog tome je sto slajder helper funkcije koriste getBoundingClientRect() koji vraca nule ako element ima
         * 'display: none'
         *
         * Info: https://stackoverflow.com/questions/42691675/how-to-getboundingclientrect-from-a-hidden-element-doesnt-work-on-ie
         */
        this.kalkulatorKredita.show();
        this.kalkulatorStednje.show();

        // Inicijalizacija slajdera.
        init(document);

        /**
         * Kada se slajderi lepo inicijalizuju na oba, odaberi jedan a drugi sakri
         */
        this.chooseKalk();
    }
}
