import { sviKrediti } from '../data/krediti';
import Kalkulator from './Kalkulator';
import Iznos from '../domain/Iznos';
import {
    getEditableRow,
    getNotEditableRow,
    getSmallerRow,
} from '../generators/htmlPartials';
import KamatnaStopa from '../domain/KamatnaStopa';
import PeriodOtplate from '../domain/PeriodOtplate';
import Ucesce from '../domain/Ucesce';
import { formatCurrency, formatPercent } from '../textHelpers';

export default class KalkulatorKredita extends Kalkulator {
    /**
     *
     * @param {HTMLDivElement} mainContainer
     * @param {string} nazivIzabraneStavke
     */
    constructor(mainContainer, nazivIzabraneStavke) {
        super(mainContainer, '-kredit', sviKrediti, nazivIzabraneStavke);
    }

    /**
     *
     * @returns {{ucesce: number | null, rata: number}}
     */
    calculate() {
        const izabraniKredit = this.izabraniPodatak();

        let kreditCeo = Number(izabraniKredit.iznos.iznosRaw);
        //console.log(izabraniKredit.iznos.iznosRaw);
        let ucesce = null;
        if (izabraniKredit.ucesce !== null) {
            if (!izabraniKredit.ucesce.isEditable) {
                ucesce = (kreditCeo * izabraniKredit.ucesce.max) / 100;
                kreditCeo = kreditCeo - ucesce;
            } else {
                ucesce = (kreditCeo * izabraniKredit.ucesce.rawValue) / 100;
                kreditCeo = kreditCeo - ucesce;
            }
        }
        const periodOtplate = Number(izabraniKredit.periodOtplate.rawPeriod);
        const anuitet = izabraniKredit.kamatnaStopa.getAnuitet(periodOtplate);
        const rata = anuitet * kreditCeo;

        return {
            ucesce,
            rata,
        };
    }

    populateScreen1() {
        const izabraniKredit = this.izabraniPodatak();
        izabraniKredit.fields.forEach((field, i) => {
            if (field instanceof Iznos) {
                const valutaRowContainer = getNotEditableRow(
                    'Valuta',
                    'valutaKredita',
                    field.valuta
                );

                const iznosKreditaRowContainer = getEditableRow({
                    labelText: 'Iznos kredita',
                    inputLabelText: 'Unesite Cifru',
                    id: i,
                    field,
                    maxValueSuffix: ` ${field.valuta}`,
                    minValueSuffix: ` ${field.valuta}`,
                });

                this.rowsContainer.appendChild(valutaRowContainer);
                this.rowsContainer.appendChild(iznosKreditaRowContainer);
                izabraniKredit.observe(
                    field,
                    document.getElementById('weightValue' + i)
                );
            } else if (field instanceof KamatnaStopa) {
                const nksRowContainer = getNotEditableRow(
                    'NKS',
                    'stopaId',
                    formatPercent(field.stopa)
                );
                this.rowsContainer.appendChild(nksRowContainer);
                izabraniKredit.observe(
                    field,
                    document.getElementById('weightValue' + i)
                );

                const eksRowContainer = getNotEditableRow(
                    'EKS',
                    'stopaEfektivna',
                    formatPercent(field.efektivna)
                );
                this.rowsContainer.appendChild(eksRowContainer);
            } else if (field instanceof PeriodOtplate) {
                const periodOtplateRowContainer = getEditableRow({
                    field,
                    labelText: 'Period otplate',
                    inputLabelText: 'Unesite period otplate',
                    id: i,
                    minValueSuffix: 'meseci',
                    maxValueSuffix: ' meseci',
                });

                this.rowsContainer.appendChild(periodOtplateRowContainer);
                izabraniKredit.observe(
                    field,
                    document.getElementById('weightValue' + i)
                );
            } else if (field instanceof Ucesce) {
                if (!field.isEditable) {
                    const row = getNotEditableRow(
                        'Učešće',
                        'ucesceValueId',
                        formatPercent(field.max)
                    );
                    this.rowsContainer.appendChild(row);
                    return;
                }

                const ucesceRowContainer = getEditableRow({
                    field,
                    labelText: 'Učešće',
                    inputLabelText: 'Unesite procenat učešća',
                    maxValueSuffix: '%',
                    minValueSuffix: '%',
                    id: i,
                });

                this.rowsContainer.appendChild(ucesceRowContainer);
                izabraniKredit.observe(
                    field,
                    document.getElementById('weightValue' + i)
                );
            }
        });
    }

    /**
     * @param {object} calculationResult
     * @param {number | null} calculationResult.ucesce
     * @param {number} calculationResult.rata
     */
    populateScreen2({ ucesce, rata }) {
        const izabraniKredit = this.izabraniPodatak();
        const rows = [];

        const row1 = getSmallerRow('Vrsta kredita', izabraniKredit.naziv);
        const row2 = getSmallerRow(
            'Iznos kredita',
            izabraniKredit.iznos.selected
        );
        const row3 = getSmallerRow(
            'Period otplate',
            izabraniKredit.periodOtplate.selected
        );
        rows.push(row1, row2, row3);
        if (ucesce !== null) {
            rows.push(
                getSmallerRow(
                    'Učešće',
                    formatCurrency(ucesce, izabraniKredit.iznos.valuta)
                )
            );
        }
        rows.push(
            getSmallerRow(
                'Iznos rate',
                formatCurrency(rata, izabraniKredit.iznos.valuta)
            )
        );
        rows.push(
            getSmallerRow(
                'NKS',
                formatPercent(izabraniKredit.kamatnaStopa.stopa)
            )
        );
        rows.push(
            getSmallerRow(
                'EKS',
                formatPercent(izabraniKredit.kamatnaStopa.stopaEfektivna)
            )
        );
        rows.forEach((row) => {
            this.screen2Rows.appendChild(row);
        });
    }
}
