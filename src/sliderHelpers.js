/**
 *
 * @param {HTMLDivElement | Document} element
 */
export function init(element) {
    const sliders = element.getElementsByClassName('tick-slider-input');

    for (let slider of sliders) {
        slider.oninput = onSliderInput;

        updateValue(slider);
        updateValuePosition(slider);
        updateLabels(slider);
        updateProgress(slider);

        setTicks(slider);
    }
}

function onSliderInput(event) {
    updateValue(event.target);
    updateValuePosition(event.target);
    updateLabels(event.target);
    updateProgress(event.target);
}

/**
 *
 * @param slider
 * @param dontUpdateInput - used to know if we are updating from input field.
 */
export function updateValue(slider, dontUpdateInput = false) {
    let value = document.getElementById(slider.dataset.valueId);
    const suffix = slider.dataset.suffix ? slider.dataset.suffix : '';
    const formatted = new Intl.NumberFormat('en-DE').format(slider.value);
    value.innerHTML = '<div>' + formatted + suffix + '</div>';
    // used to update input value from slider change
    if (!dontUpdateInput) {
        document.getElementById(slider.dataset.inputId).value = slider.value;
    }
}

export function updateValuePosition(slider) {
    let value = document.getElementById(slider.dataset.valueId);

    const percent = getSliderPercent(slider);

    const sliderWidth = slider.getBoundingClientRect().width;
    const valueWidth = value.getBoundingClientRect().width;
    const handleSize = slider.dataset.handleSize;

    let left =
        percent * (sliderWidth - handleSize) + handleSize / 2 - valueWidth / 2;

    left = Math.min(left, sliderWidth - valueWidth);
    left = slider.value === slider.min ? 0 : left;

    value.style.left = left + 'px';
}

export function updateLabels(slider) {
    const value = document.getElementById(slider.dataset.valueId);
    const minLabel = document.getElementById(slider.dataset.minLabelId);
    const maxLabel = document.getElementById(slider.dataset.maxLabelId);

    const valueRect = value.getBoundingClientRect();
    const minLabelRect = minLabel.getBoundingClientRect();
    const maxLabelRect = maxLabel.getBoundingClientRect();

    const minLabelDelta = valueRect.left - minLabelRect.left;
    const maxLabelDelta = maxLabelRect.left - valueRect.left;

    const space = 10;
    const minThreshold = minLabelRect.width + space;
    const maxThreshold = maxLabelRect.width + space;

    if (minLabelDelta < minThreshold) minLabel.classList.add('hidden');
    else minLabel.classList.remove('hidden');

    if (maxLabelDelta < maxThreshold) maxLabel.classList.add('hidden');
    else maxLabel.classList.remove('hidden');
}

export function updateProgress(slider) {
    let progress = document.getElementById(slider.dataset.progressId);
    const percent = getSliderPercent(slider);

    progress.style.width = percent * 100 + '%';
}

function getSliderPercent(slider) {
    const range = slider.max - slider.min;
    const absValue = slider.value - slider.min;

    return absValue / range;
}

function setTicks(slider) {
    let container = document.getElementById(slider.dataset.tickId);
    const spacing = parseFloat(slider.dataset.tickStep);
    const sliderRange = slider.max - slider.min;
    const tickCount = sliderRange / spacing + 1; // +1 to account for 0

    for (let ii = 0; ii < tickCount; ii++) {
        let tick = document.createElement('span');

        tick.className = 'tick-slider-tick';

        container.appendChild(tick);
    }
}

export function onResize() {
    const sliders = document.getElementsByClassName('tick-slider-input');

    for (let slider of sliders) {
        updateValuePosition(slider);
    }
}
