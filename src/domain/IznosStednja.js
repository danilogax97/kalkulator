export default class IznosStednja {
    /**
     *
     * @param {number} min
     * @param {number} max
     * @param {number} step
     * @param {string[]} valute
     */
    constructor(min, max, step, valute) {
        this.min = min;
        this.max = max;
        this.step = step;
        this.selected = max / 2;
        this.valute = valute;
    }

    get minFormatted() {
        return new Intl.NumberFormat('en-DE').format(this.min);
    }

    get maxFormatted() {
        return new Intl.NumberFormat('en-DE').format(this.max);
    }

    get iznosRaw() {
        return this.selected.split(' ')[0].replace(/\./g, '');
    }

    get valuta() {
        return this.valute[0];
    }
}
