import { formatPercent } from '../textHelpers';

class KamatnaStopa {
    constructor(stopa, config) {
        const [nominalna, efektivna] = stopa;
        this.stopa = nominalna;
        this.nominalna = nominalna;
        this.config = config;
        this.efektivna = efektivna;
    }

    get formatiranaStopa() {
        return `*${this.stopa}%`;
    }
    get formatiranaEfektivna() {
        return `*${this.efektivna}%`;
    }

    get koeficijent() {
        return 1 + this.stopa / 100 / 12;
    }

    setStopa(stopa, stopaEfektivna) {
        document.getElementById('stopaId').innerText = formatPercent(stopa);
        document.getElementById('stopaEfektivna').innerText = formatPercent(
            stopaEfektivna
        );
        this.stopa = stopa;
        this.stopaEfektivna = stopaEfektivna;
    }

    getAnuitet(periodOtplate = 12) {
        const v1 = Math.pow(this.koeficijent, periodOtplate);
        const v2 = this.koeficijent - 1;
        const v3 = Math.pow(this.koeficijent, periodOtplate);
        const result = (v1 * v2) / (v3 - 1);
        return result;
    }
}

export default KamatnaStopa;
