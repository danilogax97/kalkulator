import IznosStednja from './IznosStednja';

export class Stednja {
    /**
     *
     * @param {IznosStednja} iznos
     * @param {number[]} periodiOrocenja Niz meseci na koje moze stednja da se oroci
     * @param {string} naziv
     * @param {KamatnaStopaStednja} kamatnaStopa
     * @param {string} id
     */
    constructor(iznos, periodiOrocenja, naziv, kamatnaStopa, id) {
        this.iznos = iznos;
        this.periodiOrocenja = periodiOrocenja;
        this.naziv = naziv;
        this.kamatnaStopa = kamatnaStopa;
        this.id = id;
    }

    imaViseValuta() {
        return this.iznos.valute.length > 1;
    }

    imaVisePeriodaOrocenja() {
        return this.periodiOrocenja.length > 1;
    }

    get periodOrocenja() {
        return this.periodiOrocenja[0];
    }

    /**
     *
     * @param {string} valuta
     *
     * @returns number[]
     */
    dostupniPeriodiOrocenja(valuta) {
        return this.periodiOrocenja.filter((periodOrocenja) => {
            return this.kamatnaStopa.config[periodOrocenja].hasOwnProperty(
                valuta
            );
        });
    }

    setPeriodOrocenja = () => {};
}
