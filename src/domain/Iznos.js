class Iznos {
    constructor(min, max, step, valuta = 'RSD') {
        this.min = min;
        this.max = max;
        this.step = step;
        this.selected = max / 2;
        this.valuta = valuta;
    }

    get minFormatted() {
        return new Intl.NumberFormat('en-DE').format(this.min);
    }
    get maxFormatted() {
        return new Intl.NumberFormat('en-DE').format(this.max);
    }

    get iznosRaw() {
        return this.selected.split(' ')[0].replace(/\./g, '');
    }
}
export default Iznos;
