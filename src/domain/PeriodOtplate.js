class PeriodOtplate {
    constructor(min, max, step = 1) {
        this.min = min;
        this.max = max;
        this.step = step;
    }

    get rawPeriod() {
        return this.selected.split(' ')[0];
    }
}
export default PeriodOtplate;
