import Iznos from './Iznos';
import Ucesce from './Ucesce';
import PeriodOtplate from './PeriodOtplate';

class Kredit {
    /**
     * @param {Iznos} iznos
     * @param {PeriodOtplate} periodOtplate
     * @param {Ucesce} ucesce
     * @param {KamatnaStopa} kamatnaStopa
     * @param {string} naziv
     * @param {string} id
     */
    constructor(iznos, periodOtplate, ucesce, kamatnaStopa, naziv, id) {
        this.iznos = iznos;
        this.periodOtplate = periodOtplate;
        this.ucesce = ucesce;
        this.kamatnaStopa = kamatnaStopa;
        this.naziv = naziv;
        this.id = id;

        this.config = { childList: true };
    }

    observe = (type, target) => {
        if (type instanceof Iznos) {
            new MutationObserver(this.setIznos).observe(target, this.config);
        } else if (type instanceof Ucesce) {
            new MutationObserver(this.setUcesce).observe(target, this.config);
        } else if (type instanceof PeriodOtplate) {
            new MutationObserver(this.setPeriodOtplate).observe(
                target,
                this.config
            );
        }
    };

    setIznos = (target) => {
        const [element] = target;
        const value = element.target.innerText;
        this.iznos.selected = value;
    };

    setUcesce = (target) => {
        const [element] = target;
        const value = element.target.innerText;
        this.ucesce.selected = value;
    };

    setPeriodOtplate = (target) => {
        const [element] = target;
        const value = element.target.innerText;
        this.periodOtplate.selected = value;
        const valueRaw = Number(value.split(' ')[0]);
        const config = this.kamatnaStopa.config;
        config.forEach((con) => {
            const [string, value] = con;
            const [m1, m2] = string.split(' - ').map(Number);
            if (valueRaw >= m1 && valueRaw <= m2) {
                const [v1, v2] = value;
                this.kamatnaStopa.setStopa(v1, v2);
            }
        });
    };

    get fields() {
        const returnValue = [];
        // order here matters
        this.iznos !== null && returnValue.push(this.iznos);
        this.periodOtplate !== null && returnValue.push(this.periodOtplate);
        this.ucesce !== null && returnValue.push(this.ucesce);
        this.kamatnaStopa !== null && returnValue.push(this.kamatnaStopa);
        return returnValue;
    }
}

export default Kredit;
