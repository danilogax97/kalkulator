// Not used

import Iznos from '../domain/Iznos';
import {
    updateLabels,
    updateProgress,
    updateValue,
    updateValuePosition,
} from '../sliderHelpers';
import { getSlider } from './sliderGenerator';
import KamatnaStopa from '../domain/KamatnaStopa';
import { getNotEditableRow } from './htmlPartials';
import PeriodOtplate from '../domain/PeriodOtplate';
import Ucesce from '../domain/Ucesce';

export const handleFieldGeneration = (izabraniKredit) => {
    izabraniKredit.fields.forEach((field, i) => {
        if (field instanceof Iznos) {
            const valutaRowContainer = getNotEditableRow(
                'Valuta',
                'valutaKredita',
                field.valuta
            );

            document
                .getElementById('rows-container')
                .appendChild(valutaRowContainer);

            const rowContainer = document.createElement('div');
            rowContainer.classList.add('row');

            const label = document.createElement('div');
            label.classList.add('label', 'w-25');
            label.innerHTML = 'Iznos kredita';

            const numberInputWrapper = document.createElement('div');
            numberInputWrapper.classList.add('w-25', 'relative');

            const inputLabel = document.createElement('label');
            inputLabel.classList.add('input-label');
            inputLabel.innerHTML = 'Unesite cifru';

            const inputField = document.createElement('input');
            inputField.setAttribute('id', 'text-input' + i);
            inputField.classList.add('base-input');
            inputField.addEventListener('input', function (event) {
                const sliders = document.getElementById(
                    'tick-slider-input' + i
                );
                sliders.value = event.target.value;
                updateValue(sliders, true);
                updateValuePosition(sliders);
                updateLabels(sliders);
                updateProgress(sliders);
            });

            numberInputWrapper.appendChild(inputLabel);
            numberInputWrapper.appendChild(inputField);

            const middleSection = getSlider(field, i, {
                maxValueSuffix: ` ${field.valuta}`,
                minValueSuffix: ` ${field.valuta}`,
            });

            rowContainer.appendChild(label);
            rowContainer.appendChild(middleSection);
            rowContainer.appendChild(numberInputWrapper);

            document.getElementById('rows-container').appendChild(rowContainer);
            izabraniKredit.observe(
                field,
                document.getElementById('weightValue' + i)
            );
        } else if (field instanceof KamatnaStopa) {
            const rowContainer = getNotEditableRow(
                'NKS',
                'stopaId',
                field.formatiranaStopa
            );
            document.getElementById('rows-container').appendChild(rowContainer);
            izabraniKredit.observe(
                field,
                document.getElementById('weightValue' + i)
            );
            const rowContainer2 = getNotEditableRow(
                'EKS',
                'stopaEfektivna',
                field.formatiranaEfektivna
            );
            document
                .getElementById('rows-container')
                .appendChild(rowContainer2);
        } else if (field instanceof PeriodOtplate) {
            const rowContainer = document.createElement('div');
            rowContainer.classList.add('row');

            const label = document.createElement('div');
            label.classList.add('label', 'w-25');
            label.innerHTML = 'Period otplate';

            const numberInputWrapper = document.createElement('div');
            numberInputWrapper.classList.add('w-25', 'relative');

            const inputLabel = document.createElement('label');
            inputLabel.classList.add('input-label');
            inputLabel.innerHTML = 'Unesite period otplate';

            const inputField = document.createElement('input');
            inputField.setAttribute('id', 'text-input' + i);
            inputField.classList.add('base-input');
            inputField.addEventListener('input', function (event) {
                const sliders = document.getElementById(
                    'tick-slider-input' + i
                );
                sliders.value = event.target.value;
                updateValue(sliders, true);
                updateValuePosition(sliders);
                updateLabels(sliders);
                updateProgress(sliders);
            });

            numberInputWrapper.appendChild(inputLabel);
            numberInputWrapper.appendChild(inputField);

            const middleSection = getSlider(field, i, {
                minValueSuffix: 'meseci',
                maxValueSuffix: ' meseci',
            });

            rowContainer.appendChild(label);
            rowContainer.appendChild(middleSection);
            rowContainer.appendChild(numberInputWrapper);

            document.getElementById('rows-container').appendChild(rowContainer);
            izabraniKredit.observe(
                field,
                document.getElementById('weightValue' + i)
            );
        } else if (field instanceof Ucesce) {
            if (!field.isEditable) {
                const row = getNotEditableRow(
                    'Učešće',
                    'ucesceValueId',
                    `${field.max}%`
                );
                document.getElementById('rows-container').appendChild(row);
                return;
            }
            const rowContainer = document.createElement('div');
            rowContainer.classList.add('row');

            const label = document.createElement('div');
            label.classList.add('label', 'w-25');
            label.innerHTML = 'Učešće';

            const numberInputWrapper = document.createElement('div');
            numberInputWrapper.classList.add('w-25', 'relative');

            const inputLabel = document.createElement('label');
            inputLabel.classList.add('input-label');
            inputLabel.innerHTML = 'Unesite procenat ucesca';

            const inputField = document.createElement('input');
            inputField.setAttribute('id', 'text-input' + i);
            inputField.classList.add('base-input');
            inputField.addEventListener('input', function (event) {
                const sliders = document.getElementById(
                    'tick-slider-input' + i
                );
                sliders.value = event.target.value;
                updateValue(sliders, true);
                updateValuePosition(sliders);
                updateLabels(sliders);
                updateProgress(sliders);
            });

            numberInputWrapper.appendChild(inputLabel);
            numberInputWrapper.appendChild(inputField);

            const middleSection = getSlider(field, i, {
                maxValueSuffix: '%',
                minValueSuffix: '%',
            });

            rowContainer.appendChild(label);
            rowContainer.appendChild(middleSection);
            rowContainer.appendChild(numberInputWrapper);

            document.getElementById('rows-container').appendChild(rowContainer);
            izabraniKredit.observe(
                field,
                document.getElementById('weightValue' + i)
            );
        }
    });
};
