// Not used

import { getSelectHtml } from './initializeDOM';
import { init } from '../sliderHelpers';
import { handleFieldGeneration } from './handleFiledGeneration';

export const selectFieldGenerator = (podaci, kalkulatorId = '') => {
    podaci.forEach((podatak, index) => {
        const container = document.getElementById(
            `selection-container${kalkulatorId}`
        );
        if (index === 0) {
            document.getElementById(
                `selectedValue${kalkulatorId}`
            ).textContent = podatak.naziv;
            const newElement = document.createElement('span');
            newElement.classList.add('custom-option');
            newElement.classList.add('selected');
            newElement.textContent = podatak.naziv;
            newElement.setAttribute('data-value', podatak.naziv);
            container.appendChild(newElement);
        } else {
            const newElement = document.createElement('span');
            newElement.classList.add('custom-option');
            newElement.textContent = podatak.naziv;
            newElement.setAttribute('data-value', podatak.naziv);
            container.appendChild(newElement);
        }
    });
};

export const generateSelect = (kalkulatorId, podaci, screen) => {
    const selectHtml = getSelectHtml(kalkulatorId);

    screen.appendChild(selectHtml);

    selectFieldGenerator(podaci, kalkulatorId);

    selectEvents(screen, podaci);
};

const selectEvents = (screen, podaci, kalkulatorId) => {
    for (const option of screen.querySelectorAll('.custom-option')) {
        option.addEventListener('click', function () {
            if (!this.classList.contains('selected')) {
                this.parentNode
                    .querySelector('.custom-option.selected')
                    .classList.remove('selected');
                this.classList.add('selected');
                this.closest('.custom-select').querySelector(
                    '.custom-select__trigger span'
                ).textContent = this.textContent;
                // generisanje polja za izabrani kredit
                const nazivIzabranogPodatka = this.textContent;
                const izabraniPodatak = podaci.find(
                    (podatak) => podatak.naziv === nazivIzabranogPodatka
                );
                if (izabraniPodatak) {
                    document.getElementById(
                        `rows-container${kalkulatorId}`
                    ).innerHTML = '';
                    // kada izaberemo novi kredit opet generisi redove
                    handleFieldGeneration(izabraniPodatak);
                    init();
                }
            }
        });
    }
};
