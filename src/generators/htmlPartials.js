import {
    updateLabels,
    updateProgress,
    updateValue,
    updateValuePosition,
} from '../sliderHelpers';
import { getSlider } from './sliderGenerator';
import SelectField from './SelectField';

/**
 *
 * @param {string} label
 * @param {string} id
 * @param {string | number} number
 * @returns {HTMLDivElement}
 */
export const getNotEditableRow = (label, id, number) => {
    const rowContainer = document.createElement('div');
    rowContainer.classList.add('row', 'no-edit');

    rowContainer.innerHTML = `
		<div class="label w-25">
			${label}
		</div>
		<div class="w-75" id="${id}">
			${number}
		</div>
	`;
    return rowContainer;
};

/**
 *
 * @param {object} options
 * @param {string} options.labelText
 * @param {string} options.inputLabelText
 * @param {string} options.id
 * @param {any} options.field
 * @param {string} options.maxValueSuffix
 * @param {string} options.minValueSuffix
 */
export const getEditableRow = ({
    labelText,
    inputLabelText,
    id,
    field,
    maxValueSuffix,
    minValueSuffix,
}) => {
    const rowContainer = document.createElement('div');
    rowContainer.classList.add('row');

    const label = document.createElement('div');
    label.classList.add('label', 'w-25');
    label.innerHTML = labelText;

    const numberInputWrapper = document.createElement('div');
    numberInputWrapper.classList.add('w-25', 'relative');

    const inputLabel = document.createElement('label');
    inputLabel.classList.add('input-label');
    inputLabel.innerHTML = inputLabelText;

    const inputField = document.createElement('input');
    inputField.setAttribute('id', 'text-input' + id);
    inputField.classList.add('base-input');
    inputField.addEventListener('input', function (event) {
        const sliders = document.getElementById('tick-slider-input' + id);
        sliders.value = event.target.value;
        updateValue(sliders, true);
        updateValuePosition(sliders);
        updateLabels(sliders);
        updateProgress(sliders);
    });

    numberInputWrapper.appendChild(inputLabel);
    numberInputWrapper.appendChild(inputField);

    const middleSection = getSlider(field, id, {
        maxValueSuffix: `${maxValueSuffix}`,
        minValueSuffix: `${minValueSuffix}`,
    });

    rowContainer.appendChild(label);
    rowContainer.appendChild(middleSection);
    rowContainer.appendChild(numberInputWrapper);

    return rowContainer;
};

/**
 *
 * @param {object} options
 * @param {string} options.labelText
 * @param {string?} options.valueSuffix
 * @param {function} options.onValueChanged
 * @param {function?} options.mapping
 * @param {array} options.podaci
 * @param {string} options.id
 * @param {string} options.selectedValueId
 */
export const getSelectRow = ({
    podaci,
    labelText,
    valueSuffix,
    mapping,
    onValueChanged,
    id,
    selectedValueId,
}) => {
    const rowContainer = document.createElement('div');
    rowContainer.classList.add('row', 'p-right');

    const label = document.createElement('div');
    label.classList.add('label', 'w-25');
    label.innerHTML = labelText;

    const selectWrapper = document.createElement('div');
    selectWrapper.classList.add(valueSuffix ? 'w-50' : 'w-75', 'relative');

    const select = new SelectField(podaci, selectedValueId, onValueChanged);
    select.init(mapping);

    selectWrapper.appendChild(select.html);

    rowContainer.appendChild(label);
    rowContainer.appendChild(selectWrapper);

    if (valueSuffix) {
        const label2 = document.createElement('div');
        label2.setAttribute('id', id);
        label2.classList.add('label', 'w-25', 'pl-10');
        label2.innerHTML = valueSuffix;

        rowContainer.appendChild(label2);
    }

    return { rowContainer, select };
};

export const getSmallerRow = (label, value) => {
    const element = document.createElement('div');
    element.classList.add('row', 'smaller');

    const labelDiv = document.createElement('div');
    labelDiv.classList.add('label', 'w-25');
    labelDiv.textContent = label;

    const innerDiv = document.createElement('div');
    innerDiv.classList.add('w-70');

    const p = document.createElement('p');
    p.classList.add('p-info');
    p.textContent = value;

    innerDiv.appendChild(p);

    element.appendChild(labelDiv);
    element.appendChild(innerDiv);

    return element;
};
