export default class SelectField {
    /**
     * @param {array} podaci
     * @param {string} id
     * @param {function} onValueChanged
     * @param {number} defaultSelectedIndex Indeks onog elementa u nizu podaci koje ce biti selektovan na pocetku
     */
    constructor(podaci, id, onValueChanged, defaultSelectedIndex = 0) {
        this.podaci = podaci;
        this.id = id;
        this.onValueChanged = onValueChanged;
        this.defaultSelected = defaultSelectedIndex;

        this.createHTML();
    }

    createHTML() {
        this.html = document.createElement('div');
        this.html.classList.add('container');

        this.customSelectWrapper = document.createElement('div');
        this.customSelectWrapper.classList.add('custom-select-wrapper');

        this.customSelect = document.createElement('div');
        this.customSelect.classList.add('custom-select');

        this.customSelectTrigger = document.createElement('div');
        this.customSelectTrigger.classList.add('custom-select__trigger');

        this.selectedValueSpan = document.createElement('span');
        this.selectedValueSpan.setAttribute('id', `selectedValue${this.id}`);

        this.arrowDiv = document.createElement('div');
        this.arrowDiv.classList.add('arrow');

        this.customOptions = document.createElement('div');
        this.customOptions.classList.add('custom-options');
        this.customOptions.setAttribute('id', `selection-container${this.id}`);

        this.html.appendChild(this.customSelectWrapper);
        this.customSelectWrapper.appendChild(this.customSelect);

        this.customSelect.appendChild(this.customSelectTrigger);
        this.customSelect.appendChild(this.customOptions);

        this.customSelectTrigger.appendChild(this.selectedValueSpan);
        this.customSelectTrigger.appendChild(this.arrowDiv);
    }

    /**
     *
     * @param mapping Funkcija koja opisuje sta je ispisano u kao stavka, a sta je vrednost stavke
     */
    populate(
        mapping = (podatak) => ({ text: podatak.naziv, value: podatak.naziv })
    ) {
        this.customOptions.innerHTML = '';

        this.podaci.forEach((podatak, index) => {
            const { text, value } = mapping(podatak);

            if (index === this.defaultSelected) {
                this.selectedValueSpan.textContent = value;
                const newElement = document.createElement('span');
                newElement.classList.add('custom-option');
                newElement.classList.add('selected');

                newElement.textContent = text;
                newElement.setAttribute('data-value', value);
                this.customOptions.appendChild(newElement);
            } else {
                const newElement = document.createElement('span');
                newElement.classList.add('custom-option');
                newElement.textContent = text;
                newElement.setAttribute('data-value', value);
                this.customOptions.appendChild(newElement);
            }
        });
    }

    /**
     *
     * @param {HTMLElement} option
     */
    select(option) {
        option.parentNode
            .querySelector('.custom-option.selected')
            .classList.remove('selected');
        option.classList.add('selected');
        this.selectedValueSpan.textContent = option.textContent;
    }

    bindOptionsEvents() {
        const self = this;
        for (const option of this.customOptions.querySelectorAll(
            '.custom-option'
        )) {
            option.addEventListener('click', function () {
                if (!this.classList.contains('selected')) {
                    self.select(this);
                    self.onValueChanged(self.selectedValueSpan.textContent);
                }
            });
        }
    }

    bindEvents() {
        this.bindOptionsEvents();

        const self = this;

        // Zatvara dropdown kada se klikne bilo gde drugde u prozoru.
        window.addEventListener('click', function (e) {
            if (!self.customSelect.contains(e.target)) {
                self.customSelect.classList.remove('open');
            }
        });

        // Otvara dropdown kada se klikne na njega.
        this.customSelectWrapper.addEventListener('click', function () {
            self.customSelect.classList.toggle('open');
        });
    }

    /**
     *
     * @returns {string} Naziv podatka
     */
    getSelected() {
        return this.selectedValueSpan.textContent;
    }

    init(
        mapping = (podatak) => ({ text: podatak.naziv, value: podatak.naziv })
    ) {
        this.populate(mapping);
        this.bindEvents();
    }
}
