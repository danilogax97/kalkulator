export const getSlider = (
    field,
    i,
    { minValue, maxValueSuffix, minValueSuffix } = {},
    format = null
) => {
    const middleSection = document.createElement('div');
    middleSection.classList.add('w-50', 'm-r-20');

    const sliderContainer = document.createElement('div');
    sliderContainer.classList.add('sliderContainer');

    const tickSlider = document.createElement('div');
    tickSlider.classList.add('tick-slider');

    const tickSliderValueContainer = document.createElement('div');
    tickSliderValueContainer.classList.add('tick-slider-value-container');

    const tick1 = document.createElement('div');
    tick1.setAttribute('id', 'weightLabelMin' + i);
    tick1.classList.add('tick-slider-label');
    tick1.innerHTML =
        minValue ??
        (field.minFormatted ?? field.min) +
            (minValueSuffix ? ` ${minValueSuffix}` : '');

    const tick2 = document.createElement('div');
    tick2.setAttribute('id', 'weightLabelMax' + i);
    tick2.classList.add('tick-slider-label');
    tick2.innerHTML =
        (field.maxFormatted ?? field.max) +
        (maxValueSuffix ? `${maxValueSuffix}` : '');

    const tick3 = document.createElement('div');
    tick3.setAttribute('id', 'weightValue' + i);
    tick3.classList.add('tick-slider-value');

    tickSliderValueContainer.appendChild(tick1);
    tickSliderValueContainer.appendChild(tick3);
    tickSliderValueContainer.appendChild(tick2);

    const tickSliderBackGround = document.createElement('div');
    tickSliderBackGround.classList.add('tick-slider-background');

    const weightProgress = document.createElement('div');
    weightProgress.classList.add('tick-slider-progress');
    weightProgress.setAttribute('id', 'weightProgress' + i);

    const weightTicks = document.createElement('div');
    weightTicks.classList.add('tick-slider-tick-container');
    weightTicks.setAttribute('id', 'weightTicks' + i);

    const input = document.createElement('input');
    input.classList.add('tick-slider-input');
    input.setAttribute('id', 'tick-slider-input' + i);
    input.setAttribute('type', 'range');
    input.setAttribute('min', field.min);
    input.setAttribute('max', field.max);
    input.setAttribute('step', field.step);
    input.setAttribute('value', field.selected);
    input.setAttribute('data-tick-step', field.step);
    input.setAttribute('data-tick-id', 'weightTicks' + i);
    input.setAttribute('data-value-id', 'weightValue' + i);
    input.setAttribute('data-progress-id', 'weightProgress' + i);
    input.setAttribute('data-handle-size', '18');
    input.setAttribute('data-min-label-id', 'weightLabelMin' + i);
    input.setAttribute('data-max-label-id', 'weightLabelMax' + i);
    input.setAttribute('data-input-id', 'text-input' + i);
    maxValueSuffix && input.setAttribute('data-suffix', maxValueSuffix);

    tickSlider.appendChild(tickSliderValueContainer);
    tickSlider.appendChild(tickSliderBackGround);
    tickSlider.appendChild(weightProgress);
    tickSlider.appendChild(weightTicks);
    tickSlider.appendChild(input);

    sliderContainer.appendChild(tickSlider);

    middleSection.appendChild(sliderContainer);

    return middleSection;
};
