// not used

import { sviKrediti } from './data/krediti';

const getElement = (label, value) => {
    const element = document.createElement('div');
    element.classList.add('row', 'smaller');
    element.innerHTML = `<div class="label" style="width: 25%;">${label}</div>
							<div style="width: 70%">
								<p class="p-info">
									${value}
								</p>
							</div>`;
    return element;
};
const generateResult = (izabraniKredit, rata, ucesce) => {
    const format = new Intl.NumberFormat('en-DE');
    const containter = document.getElementById('screen2-rows');
    containter.innerHTML = '';
    const rows = [];

    const row1 = getElement('Vrsta kredita', izabraniKredit.naziv);
    const row2 = getElement('Iznos kredita', izabraniKredit.iznos.selected);
    const row3 = getElement(
        'Period otplate',
        izabraniKredit.periodOtplate.selected
    );
    rows.push(row1, row2, row3);
    if (ucesce !== null) {
        rows.push(
            getElement(
                'Učešće',
                `${format.format(ucesce)} ${izabraniKredit.iznos.valuta}`
            )
        );
    }
    rows.push(
        getElement(
            'Iznos rate',
            `${format.format(rata)} ${izabraniKredit.iznos.valuta}`
        )
    );
    rows.push(getElement('NKS', `${izabraniKredit.kamatnaStopa.stopa}%`));
    rows.push(
        getElement('EKS', `${izabraniKredit.kamatnaStopa.stopaEfektivna}%`)
    );
    rows.forEach((row) => {
        containter.appendChild(row);
    });
};
export const switchView = function () {
    let view1 = document.getElementById('screen1');
    let view2 = document.getElementById('screen2');
    if (view1.classList.contains('show')) {
        view1.classList.replace('show', 'hide');
        view2.classList.replace('hide', 'show');

        const nazivIzabranogKredita = document.getElementById('selectedValue')
            .textContent;
        const izabraniKredit = sviKrediti.find(
            (kredit) => kredit.naziv === nazivIzabranogKredita
        );

        let kreditCeo = Number(izabraniKredit.iznos.iznosRaw);
        let ucesce = null;
        if (izabraniKredit.ucesce !== null) {
            if (!izabraniKredit.ucesce.isEditable) {
                ucesce = (kreditCeo * izabraniKredit.ucesce.max) / 100;
                kreditCeo = kreditCeo - ucesce;
            } else {
                ucesce = (kreditCeo * izabraniKredit.ucesce.rawValue) / 100;
                kreditCeo = kreditCeo - ucesce;
            }
        }
        const periodOtplate = Number(izabraniKredit.periodOtplate.rawPeriod);
        const anuitet = izabraniKredit.kamatnaStopa.getAnuitet(periodOtplate);
        const rata = anuitet * kreditCeo;

        generateResult(izabraniKredit, rata, ucesce);
        console.log(rata);
    } else {
        view2.classList.replace('show', 'hide');
        view1.classList.replace('hide', 'show');
    }
};
