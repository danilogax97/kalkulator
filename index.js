import './src/css/index.css';
import './src/css/range-input.css';
import './src/css/styles.css';
import KalkulatorSwitcher from './src/kalkulatori/KalkulatorSwitcher';

const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
if (isFirefox) {
    require('./src/css/styled-override.css');
}

// izabraniKalkulator: "kalkulator-kredita" ili "kalkulator-stednje" ili "kalkulator-zaduzenja"
/**
 * @param options
 * @param {string} options.target
 * @param {("kalkulator-kredita" | "kalkulator-stednje" | "kalkulator-zaduzenja")} izabraniKalkulator
 * @param {string} izabranaStavka
 */
export const initialize = ({ target, izabraniKalkulator, izabranaStavka }) => {
    /**
     * @type {HTMLDivElement}
     */
    const mainContainer = document.querySelector(target);
    const switcher = new KalkulatorSwitcher(
        mainContainer,
        izabraniKalkulator,
        izabranaStavka
    );
    switcher.initKalks();
};
