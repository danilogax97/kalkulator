const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    watch: true,
    entry: ['./index.js'],
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            { test: /\.css$/i, use: ['style-loader', 'css-loader'] },
        ],
    },
    resolve: {
        extensions: ['.js'],
    },
    output: {
        path: `${__dirname}/public`,
        publicPath: '/',
        filename: 'app.js',
        library: 'Kalkulator',
    },
    plugins: [
        new webpack.DefinePlugin({
            __DEV__: true,
        }),
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        liveReload: true,
        port: 9000,
        historyApiFallback: true,
        writeToDisk: true,
    },
};
